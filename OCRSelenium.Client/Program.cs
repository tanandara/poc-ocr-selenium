﻿using OCRSelenium.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCRSelenium.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            IRunner selenium = new SeleniumRunner();
            selenium.Run();

            Console.ReadKey();
        }
    }
}
