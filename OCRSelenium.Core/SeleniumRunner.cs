﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tesseract;

namespace OCRSelenium.Core
{
    public interface IRunner
    {
        void Run();
    }
    public class SeleniumRunner : IRunner
    {
        private readonly ChromeDriver _driver;

        public SeleniumRunner()
        {
            this._driver = new ChromeDriver();
        }

        public void Run()
        {
            this._driver
                .Navigate()
                .GoToUrl("https://ag.ufabet.com");

            Thread.Sleep(1500);
            string base64string = GetBase64ContentFromImage();

            Thread.Sleep(250);
            string filePath = SaveImageByBase64Content(base64string);

            Thread.Sleep(100);
            string captchaText = ResolveCaptchaByOCR(filePath);

            FillCaptchaToTextbox(captchaText);

        }

        private void FillCaptchaToTextbox(string captchaText)
        {
            var captchaTextBox = this._driver.FindElementById("txtCode");
            var captchaInput = captchaText
                                .Replace("\\0", "")
                                .Replace("\\\n", "");
            var evalExecute = string.Format("document.querySelector('#txtCode').value = {0};", captchaInput);

            // for demo p'bob
            this._driver.ExecuteScript(evalExecute);

            // SendKeys() is type captcha into textbox and enter
            // captchaTextBox.SendKeys(captchaInput);
        }

        private string ResolveCaptchaByOCR(string filePath)
        {
            var captchaText = "";
            using (var engine = new TesseractEngine(@"tessdata/", "eng"))
            {
                engine.SetVariable("tessedit_char_whitelist", "0123456789");

                using (var img = Pix.LoadFromFile(filePath))
                using (var page = engine.Process(img, PageSegMode.SingleChar))
                using (var iterator = page.GetIterator())
                {
                    captchaText = page.GetText();
                    Console.WriteLine(captchaText);
                    iterator.Begin();

                    do
                    {
                        var text = iterator.GetText(PageIteratorLevel.Word);
                        Console.WriteLine(int.Parse(text));
                    }
                    while (iterator.Next(PageIteratorLevel.Word));
                }
            }

            return captchaText;
        }

        private string SaveImageByBase64Content(string base64string)
        {
            var base64 = base64string.Split(',').Last();
            var filePath = "";
            using (var stream = new MemoryStream(Convert.FromBase64String(base64)))
            using (var bitmap = new Bitmap(stream))
            {
                filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ImageName.gif");
                bitmap.Save(filePath, System.Drawing.Imaging.ImageFormat.Gif);
            }

            return filePath;
        }

        private string GetBase64ContentFromImage()
        {
            // https://stackoverflow.com/questions/18424624/using-selenium-to-save-images-from-page
            return this._driver.ExecuteScript(@"
            var c = document.createElement('canvas');
            var ctx = c.getContext('2d');
            var img = document.querySelector('#divImgCode > img');
            c.height=img.naturalHeight;
            c.width=img.naturalWidth;
            ctx.drawImage(img, 0, 0,img.naturalWidth, img.naturalHeight);
            var base64String = c.toDataURL();
            return base64String;
            ") as string;
        }
    }
}
